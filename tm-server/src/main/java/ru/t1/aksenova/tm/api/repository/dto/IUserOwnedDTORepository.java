package ru.t1.aksenova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends IDTORepository<M> {

    M add(@NotNull final String userId, @NotNull final M model);

    void update(@NotNull final String userId, @NotNull final M model);

    void remove(@NotNull final String userId, @NotNull final M model);

}
