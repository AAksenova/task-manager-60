package ru.t1.aksenova.tm.service.model;


import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.repository.model.ISessionRepository;
import ru.t1.aksenova.tm.api.service.model.ISessionService;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.exception.entity.SessionNotFoundException;
import ru.t1.aksenova.tm.exception.field.DateEmptyException;
import ru.t1.aksenova.tm.exception.field.IdEmptyException;
import ru.t1.aksenova.tm.exception.field.UserIdEmptyException;
import ru.t1.aksenova.tm.exception.user.RoleEmptyException;
import ru.t1.aksenova.tm.model.Session;
import ru.t1.aksenova.tm.model.User;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository repository;

    @NotNull
    @Override
    public ISessionRepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    public Session create(
            @Nullable final String userId,
            @Nullable final String role
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (role == null || role.isEmpty()) throw new RoleEmptyException();
        @NotNull Session session = new Session();
        session.setRole(Role.valueOf(role));
        session.setUser(getRepository().getEntityManager().find(User.class, userId));
        add(session);
        return session;
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findAll(userId);
    }

    @Nullable
    @Override
    public Session findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session session = getRepository().findOneById(userId, id);
        if (session == null) throw new SessionNotFoundException();
        return session;
    }

    @Override
    @Transactional
    public void clear() {
        getRepository().clear();
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().clear(userId);
    }

    @Nullable
    @Override
    public Session removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session session = findOneById(userId, id);
        remove(userId, session);
        return session;
    }

    @NotNull
    @Override
    public Session updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String role,
            @Nullable final Date date
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (role == null || role.isEmpty()) throw new RoleEmptyException();
        if (date == null) throw new DateEmptyException();
        @Nullable final Session session = findOneById(userId, id);
        if (session == null) throw new SessionNotFoundException();
        session.setRole(Role.valueOf(role));
        session.setDate(date);
        update(userId, session);
        return session;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return (getRepository().findOneById(userId, id) != null);

    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().getCount(userId);
    }

}
