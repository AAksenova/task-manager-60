package ru.t1.aksenova.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.aksenova.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.aksenova.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.aksenova.tm.exception.field.UserIdEmptyException;

@Service
@AllArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedDTORepository<M>>
        extends AbstractDTOService<M, R> implements IUserOwnedDTOService<M> {

    @Nullable
    @Override
    @Transactional
    public M add(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        return getRepository().add(userId, model);
    }

    @Override
    @Transactional
    public void update(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        getRepository().update(userId, model);
    }

    @Override
    @Transactional
    public void remove(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        getRepository().remove(userId, model);
    }

}
