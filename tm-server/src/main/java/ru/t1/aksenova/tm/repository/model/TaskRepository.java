package ru.t1.aksenova.tm.repository.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.aksenova.tm.api.repository.model.ITaskRepository;
import ru.t1.aksenova.tm.enumerated.TaskSort;
import ru.t1.aksenova.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@AllArgsConstructor
public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAll() {
        @NotNull final String jpql = "SELECT m FROM Task m";
        return entityManager.createQuery(jpql, Task.class).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        if (userId.isEmpty() || userId == null) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final TaskSort sort) {
        if (userId.isEmpty() || userId == null) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId ORDER BY m." +
                getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final Comparator<Task> comparator) {
        if (userId.isEmpty() || userId == null) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId ORDER BY m." +
                getSortType(comparator);
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String id) {
        if (id.isEmpty() || id == null) return null;
        return entityManager.find(Task.class, id);
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty() || id == null) return null;
        if (userId.isEmpty() || userId == null) return null;
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId and m.id = :id";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String id) {
        Optional<Task> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        Optional<Task> model = Optional.ofNullable(findOneById(id, userId));
        model.ifPresent(this::remove);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM Task";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM Task m WHERE m.user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public long getCount() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Task m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getCount(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Task m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public boolean existById(@NotNull final String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM Task m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM Task m WHERE m.id = :id AND m.user.id = :userId";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty() || userId == null) return Collections.emptyList();
        if (projectId.isEmpty() || projectId == null) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.project.id = :projectId AND m.user.id = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeTaskByProjectId(@NotNull final String projectId) {
        if (projectId.isEmpty() || projectId == null) return;
        @NotNull final String jpql = "DELETE FROM Task m WHERE m.project.id = :projectId";
        entityManager.createQuery(jpql, Task.class)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
