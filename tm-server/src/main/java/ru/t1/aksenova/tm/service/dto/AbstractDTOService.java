package ru.t1.aksenova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.repository.dto.IDTORepository;
import ru.t1.aksenova.tm.api.service.dto.IDTOService;
import ru.t1.aksenova.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>>
        implements IDTOService<M> {

    @Getter
    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected abstract IDTORepository<M> getRepository();

    @NotNull
    @Override
    @Transactional
    public M add(@NotNull final M model) {
        getRepository().add(model);
        return model;
    }

    @Override
    @Transactional
    public void update(@NotNull M model) {
        getRepository().update(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull M model) {
        getRepository().remove(model);
    }

}
