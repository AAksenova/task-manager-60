package ru.t1.aksenova.tm.api.repository.model;

import ru.t1.aksenova.tm.model.AbstractModel;

import javax.persistence.EntityManager;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    void update(M model);

    void remove(M model);

    EntityManager getEntityManager();

}
