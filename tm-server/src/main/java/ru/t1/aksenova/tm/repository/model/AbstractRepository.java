package ru.t1.aksenova.tm.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.repository.model.IRepository;
import ru.t1.aksenova.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Getter
@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    @Transactional
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        entityManager.remove(entityManager.merge(model));
    }

}
