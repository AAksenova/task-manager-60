package ru.t1.aksenova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAdminService {

    @NotNull
    @SneakyThrows
    String getDropScheme(@Nullable String token);

    @NotNull
    @SneakyThrows
    String getInitScheme(@Nullable String token);

}
