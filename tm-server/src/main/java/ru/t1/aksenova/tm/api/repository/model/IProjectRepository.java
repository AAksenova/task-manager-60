package ru.t1.aksenova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.ProjectSort;
import ru.t1.aksenova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@NotNull String userId);

    @NotNull
    List<Project> findAll(@NotNull String userId, @NotNull ProjectSort sort);

    @NotNull
    List<Project> findAll(@NotNull String userId, @NotNull Comparator<Project> comparator);

    @Nullable
    Project findOneById(@NotNull String id);

    @Nullable
    Project findOneById(@NotNull String id, @NotNull String userId);

    void removeById(@NotNull String id);

    void removeById(@NotNull String id, @NotNull String userId);

    void clear();

    void clear(@NotNull String userId);

    long getCount();

    long getCount(@NotNull String userId);

    boolean existById(@NotNull String id);

    boolean existById(@NotNull String id, @NotNull String userId);

}
