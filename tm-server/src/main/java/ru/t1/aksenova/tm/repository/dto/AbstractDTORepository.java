package ru.t1.aksenova.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.repository.dto.IDTORepository;
import ru.t1.aksenova.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Getter
@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTORepository<M extends AbstractModelDTO> implements IDTORepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    @Transactional
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        entityManager.remove(entityManager.merge(model));
    }

}
