package ru.t1.aksenova.tm.api.service.dto;

import ru.t1.aksenova.tm.api.repository.dto.IDTORepository;
import ru.t1.aksenova.tm.dto.model.AbstractModelDTO;

public interface IDTOService<M extends AbstractModelDTO> extends IDTORepository<M> {
}
