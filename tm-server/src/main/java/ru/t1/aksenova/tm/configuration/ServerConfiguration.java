package ru.t1.aksenova.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.aksenova.tm.api.service.IPropertyService;

import javax.sql.DataSource;
import java.util.Properties;

import static org.hibernate.cfg.AvailableSettings.*;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.aksenova.tm")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;


    @Bean
    @NotNull
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDatabaseDriver());
        dataSource.setUrl(propertyService.getDatabaseConnection());
        dataSource.setUsername(propertyService.getDatabaseUser());
        dataSource.setPassword(propertyService.getDatabasePassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.aksenova.tm");
        @NotNull final Properties properties = new Properties();
        properties.put(DIALECT, propertyService.getDatabaseHibernateDialect());
        properties.put(HBM2DDL_AUTO, propertyService.getDatabaseHibernateHbm2ddl());
        properties.put(SHOW_SQL, propertyService.getDatabaseHibernateShowSql());
        properties.put(FORMAT_SQL, propertyService.getDatabaseHibernateFormatSql());
        properties.put(USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseCacheUseSecondLevelCache());
        properties.put(CACHE_REGION_FACTORY, propertyService.getDatabaseCacheRegionFactoryClass());
        properties.put(USE_QUERY_CACHE, propertyService.getDatabaseCacheUseQueryCache());
        properties.put(USE_MINIMAL_PUTS, propertyService.getDatabaseCacheUseMinimalPuts());
        properties.put(CACHE_REGION_PREFIX, propertyService.getDatabaseCacheRegionPrefix());
        properties.put(CACHE_PROVIDER_CONFIG, propertyService.getDatabaseCacheProviderConfigFileResourcePath());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }


}
