package ru.t1.aksenova.tm.exception.field;

import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.exception.AbstractException;

public abstract class AbstractFieldException extends AbstractException {

    public AbstractFieldException() {
    }

    public AbstractFieldException(@Nullable final String message) {
        super(message);
    }

    public AbstractFieldException(
            @Nullable final String message,
            @Nullable final Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractFieldException(@Nullable final Throwable cause) {
        super(cause);
    }

    public AbstractFieldException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
