package ru.t1.aksenova.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class DropSchemeRequest extends AbstractUserRequest {

    public DropSchemeRequest(@Nullable final String token) {
        super(token);
    }

}
