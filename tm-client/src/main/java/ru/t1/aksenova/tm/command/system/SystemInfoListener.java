package ru.t1.aksenova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.event.ConsoleEvent;
import ru.t1.aksenova.tm.util.FormatUtil;

@Component
public final class SystemInfoListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-i";

    @NotNull
    public static final String NAME = "info";

    @NotNull
    public static final String DESCRIPTION = "Show system information.";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@systemInfoListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        @NotNull final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        @NotNull final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        @NotNull final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        @NotNull final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);

        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        @NotNull final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;

        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

}
