package ru.t1.aksenova.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    @NotNull
    void setStatus(@NotNull Status status);

}
