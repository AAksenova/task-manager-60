package ru.t1.aksenova.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.event.ConsoleEvent;

public interface ICommand {

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @Nullable
    String getName();

    void handler(@NotNull ConsoleEvent consoleEvent);

    @Nullable
    Role[] getRoles();

}
