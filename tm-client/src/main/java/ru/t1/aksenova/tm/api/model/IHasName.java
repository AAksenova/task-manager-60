package ru.t1.aksenova.tm.api.model;

import org.jetbrains.annotations.NotNull;

public interface IHasName {

    @NotNull
    String getName();

    @NotNull
    void setName(@NotNull String name);

}
